import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { TagSynonymItemModel } from '../models';
import { HttpService } from '../services/http.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

  synonyms$: Observable<TagSynonymItemModel[]>;

  constructor(private httpService: HttpService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.paramMap.subscribe(x => {
      const tag = x.get('tag');
      this.synonyms$ = this.httpService.getTagSynonyms(tag);
    });
  }

}
