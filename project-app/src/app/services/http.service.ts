import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { TagItemModel, ItemsModel, TagSynonymItemModel } from '../models';
import { switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  private baseUrl = 'https://api.stackexchange.com/2.2/';

  constructor(private http: HttpClient) { }

  getTags(page = 1): Observable<TagItemModel[]> {
    const url = `tags?page=${page}&order=desc&sort=popular&site=stackoverflow`;
    return this.http.get<ItemsModel<TagItemModel[]>>(this.baseUrl + url).pipe(
      switchMap(x => of(x.items))
    );
  }

  getTagSynonyms(tag: string, page = 1): Observable<TagSynonymItemModel[]> {
    const url = `tags/${tag}/synonyms?page=${page}&order=desc&sort=creation&site=stackoverflow`;
    return this.http.get<ItemsModel<TagItemModel[]>>(this.baseUrl + url).pipe(
      switchMap(x => {
        const items = x['items'];
        const result = [];
        items.forEach(item => {
          const synonym: TagSynonymItemModel = {
            fromTag: item['from_tag']
          };
          result.push(synonym);
        });
        return of(result);
      })
    );
  }
}
