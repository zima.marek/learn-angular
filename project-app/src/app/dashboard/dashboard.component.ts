import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { flatMap, switchMap } from 'rxjs/operators';
import { TagItemModel } from '../models';
import { HttpService } from '../services/http.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  page = 1;

  tags$: Observable<TagItemModel[]>;

  constructor(private httpService: HttpService) { }

  ngOnInit() {
    this.tags$ = this.httpService.getTags();
  }

  onNext() {
    this.page += 1;
    this.tags$ = this.httpService.getTags(this.page);
  }

  onPrevious() {
    this.page = this.page > 1 ? this.page -= 1 : this.page;
    this.tags$ = this.httpService.getTags(this.page);
  }

}
